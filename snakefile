## To run this on a cluster:
## snakemake -j 10 -c 20 --cluster "qsub -V -l h_rt=70:00:00 -l h_vmem=120G" --latency-wait 30 --rerun-incomplete --use-conda --conda-prefix env --conda-frontend conda

## Use Snakemake version >5.10 as this has LINT functionality.
## Running snakemake for the first time will install all packages in the conda environment.
## May take some time.
## Using mamba is better and faster but for some reason it is very slow to install on Eddie.

import os
import glob

## Load config file.
configfile: "conf/config.yaml"

## Define input wildcards:
## In this case, the directory containing the pod5 files.
## The basename is the ID of the sample.
IDS = os.listdir(config["POD5_DIR"])

## Target:
rule all:
    input:
        expand(config["BAM_DIR"] + "/dorado/{id}/" + "{id}.bam", id=IDS),
        expand(config["BAM_DIR"] + "/dorado/{id}/" + "{id}.bam.bai", id=IDS)


## Step 2: Take pod5 file, basecall and align to reference genome using Dorado (embedded alignment step using minimap2)
rule step2_dorado:
    message:
        "Step 2: Running dorado on {wildcards.id}. This will perform basecalling and aligning to reference genome using minimap2 ..."
    input:
        config["POD5_DIR"] + "/{id}/{id}.merged.pod5"
    output:
        fastq = config["FASTQ_DIR"] + "/dorado/{id}/" + "{id}.fastq",
        bam = config["BAM_DIR"] + "/dorado/{id}/" + "{id}.bam",
        index = config["BAM_DIR"] + "/dorado/{id}/" + "{id}.bam.bai"                
    log:
        config["LOG_DIR"] + "/dorado/{id}.log"
    benchmark:
        config["BENCHMARK_DIR"] + "/dorado/{id}.benchmark"
    conda:
        config["CONDA_ENV"]
    threads:
        20
    resources:
        mem_mb = 120000,
        h_rt = "24:00:00"
    shell:
        """
        echo "Running dorado on {wildcards.id} ..."
        ####DORADO=/exports/igmm/eddie/Glioblastoma-WGS/scripts/dorado-0.3.0-linux-x64/bin/dorado

        ## Run once:
        ## $DORADO donwload (this will download all the available models)
        ###MODEL_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/dorado-0.3.0-linux-x64/models
        ###MODEL_OPTION=dna_r10.4.1_e8.2_400bps_sup@v4.2.0
        ###REFERENCE=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa
        
        ## Run dorado:
        ###($DORADO basecaller $MODEL_DIR/$MODEL_OPTION {input} --device "cpu" --emit-fastq > {output.fastq} && \        
        ####$DORADO aligner -t {threads} $REFERENCE {output.fastq} > {output.bam}) 2> {log}

        """
    




## Step 3: QC metrics on fastq and bam files:
## rule step3_qc_metrics:




## Step 4: Coverage metrics:
## rule step4_coverage_metrics:




## Step 5: Call methylation using remora







## Take bam file, call SNVs using clairS.
## Take bam file, phase variants using whatshap
## Take bam file, call CNVs using HATCHet
## Take bam file, call SVs using nanomonSV

