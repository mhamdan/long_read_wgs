# Long read WGS pipeline

This repository contains scripts and workflows on the latest methods to analyse long read WGS data, predominantly focusing on Oxford Nanopore reads. It runs the pipeline from fast5 files onwards (but in the future can plug in already generated pod5 format). The pipeline is run using Snakemake version at least 7.25

## Step 1: Convert Fast5 to POD5 format (if not done already in MinKnow)
- Based on https://github.com/nanoporetech/pod5-file-format

## Step 2: Basecalling using Dorado (simultaneous alignment using minimap2)
- Based on https://github.com/nanoporetech/dorado
- This performs basecalling and then run minimap2 to align against a reference genome (--ax map-ont option)
- This will generate bam file for each sample (and also a fastq file)
- We use super accuracy model whenever possible
- Minion flowcell currently in use is R10.4.1 (Kit 14)

## Step 3: QC metrics (on FASTQ and BAM files)


## Step 4: Calculate coverage statistics using mosdepth


## Step 5: Call SNVs using Clair3 and phase them using Whatshap
- Based on https://github.com/HKU-BAL/Clair3
- If has normal bam file, then use ClairS as per https://github.com/HKU-BAL/ClairS

## Step 6: Call CNVs using HATCHet
## Step 7: Call SVs using nanomonSV
## Step 8: Assemble ecDNA contigs using Flye
## Step 9: Call methylation status using Remora
## Step N: Call base modification status using modkit (uncertain of utility)