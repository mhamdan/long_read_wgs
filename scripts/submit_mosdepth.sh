#!/bin/bash

#$ -N mosdepth
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=8G
#$ -pe sharedmem 8
#$ -l h_rt=64:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

CONFIG=$1
SAMPLE_ID=$2

## Use mosdepth, based on https://github.com/brentp/mosdepth
## Need version 0.2.9 which in turn needs py2.
## Install within py2 conda
## Loaded with source $CONFIG

source $CONFIG 

INPUT_SORTED_BAM=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/bams/dorado/${SAMPLE_ID}/${SAMPLE_ID}_sorted.bam
MOSDEPTH_QC_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/qc/mosdepth/${SAMPLE_ID}

if [[ ! -d $MOSDEPTH_QC_DIR ]]; then
    echo "Creating output directory for mosdepth QC file"
    mkdir $MOSDEPTH_QC_DIR
else
    echo "Output directory for mosdepth QC file already exists"
fi

echo "Running mosdepth QC on ${SAMPLE_ID}"
cd $MOSDEPTH_QC_DIR && $MOSDEPTH --fast-mode -t 4 -Q 20 $SAMPLE_ID $INPUT_SORTED_BAM 
echo "Finished running mosdepth QC on ${SAMPLE_ID}"








