#!/bin/bash

## This script splits N number of files into folders containing N number of files

#$ -N split_fast5
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=64G
#$ -l h_rt=3:00:00

## Define SGE variables:
ID=$1
FAST5_DIR=$2
N=$3 ## number of files per subfolder
PARAM_DIR=$4

#### To split files into subfolders to enable running in parallel:
## N refers to number of files per subfolder

ORIGINAL_DIR=$FAST5_DIR/$ID

cd $ORIGINAL_DIR
for f in *; 
do 
    d=split_$(printf %03d $((i/$N+1)))
    mkdir -p $d
    mv "$f" $d
    let i++
done

# Create a txt file containing folder and path to folder
folders=$(ls $ORIGINAL_DIR)

# Loop through the folders and add them to the table, without headings.
for folder in $folders; do
    path="$ORIGINAL_DIR/$folder"
    echo $folder $path >> $PARAM_DIR/${ID}_split_fast5_list.txt
done


