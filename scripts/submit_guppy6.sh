#!/bin/bash

#$ -N guppy6
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -pe gpu-titanx 1
#$ -l h_vmem=100G
#$ -l h_rt=24:00:00

### These exact settings work, don't change.

. /etc/profile.d/modules.sh
module load igmm/apps/guppy/6.4.6.gpu

## Define SGE variables:
SAMPLE_NAME=$1
IDS=$2

## Get the ID and path to the split fast5 folder:
SPLIT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
SPLIT_FAST5_DIR=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
GUPPY646_OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/raw/fastq/guppy646/${SAMPLE_NAME}/${SPLIT_ID}/

## Define paths:
MODE=dna_r10.4.1_e8.2_400bps_sup.cfg
CONFIG_FILE=/exports/igmm/software/pkg/el7/apps/guppy/6.4.6.gpu/data/$MODE

## Run guppy646:
echo "Running guppy version 6.4.6 basecalling..."

## Create a directory for sanmple name if it doesn't exist:
if [[ ! -d $GUPPY646_OUTPUT_DIR ]]; then
    mkdir -p $GUPPY646_OUTPUT_DIR
fi

## Declare paramaters:
echo $SPLIT_FAST5_DIR
echo $GUPPY646_OUTPUT_DIR
echo $CONFIG_FILE

guppy_basecaller \
    -i $SPLIT_FAST5_DIR \
    -s $GUPPY646_OUTPUT_DIR \
    -c $CONFIG_FILE \
    --verbose_logs \
    --device auto \
    --chunk_size 1000 \
    --verbose_logs \
    --compress_fastq \
    --gpu_runners_per_device 1

