#!/bin/bash

#$ -N guppy5
#$ -cwd
#$ -pe gpu-titanx 1
#$ -l h_vmem=32G
#$ -l h_rt=2:00:00

# Initialise the environment modules and load guppy version 5.0.11
. /etc/profile.d/modules.sh
module load roslin/guppy/5.0.11-gpu

## Define SGE variables:
SAMPLE_NAME=$1
IDS=$2

## Get the ID and path to the split fast5 folder:
SPLIT_ID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 1`
SPLIT_FAST5_DIR=`head -n $SGE_TASK_ID $IDS | tail -n 1 | cut -f 2`
GUPPY5_OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/raw/fastq/guppy5/${SAMPLE_NAME}/${SPLIT_ID}

## Define model and config file:
MODE=dna_r10.3_450bps_sup.cfg
CONFIG_FILE=/exports/applications/apps/community/roslin/guppy/5.0.11-gpu/data/$MODE

## Run guppy 5.0.11:
echo "Running guppy version 5.0.11 basecalling..."

## Create a directory for sanmple name if it doesn't exist:
if [[ ! -d $GUPPY5_OUTPUT_DIR ]]; then
    mkdir -p $GUPPY5_OUTPUT_DIR
fi

guppy_basecaller \
    -i $SPLIT_FAST5_DIR \
    -s $GUPPY5_OUTPUT_DIR \
    -c $CONFIG_FILE \
    --device auto \
    --verbose_logs \
    --num_callers 2 \
    --compress_fastq

    