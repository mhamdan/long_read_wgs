#!/bin/bash

#$ -N dorado
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -pe gpu-a100 1
#$ -l h_vmem=150G
#$ -l h_rt=24:00:00

## When running this script, using A100, need at least h_vmem=150G
## Initialise the environment modules and load A100 compatible CUDA version 12.1.1
. /etc/profile.d/modules.sh
module load cuda/12.1.1

## Define SGE variables:
CONFIG=$1
ID=$2
MODEL_OPTION=$3

## Either dna_r10.4.1_e8.2_400bps_sup@v4.0.0 or the 9.4 equivalent

source $CONFIG

## Get the models, run once:
## $DORADO donwload (this will download all the available models)
## Need to be careful what model you run, as 4kHz data can't be run for models used for 5kHz data.
## Refer to https://github.com/nanoporetech/dorado for more information.

## Run dorado:
if [[ ! -d $DORADO_OUTPUT_DIR ]]; then
    echo "Creating output directory for fastq file"
    mkdir $DORADO_OUTPUT_DIR
else
    echo "Output directory for fastq file already exists"
fi

if [[ ! -f $DORADO_OUTPUT_FASTQ ]]; then
    echo "Running dorado basecaller on pod5 data, emitting fastq file..."
    $DORADO basecaller --emit-fastq $MODEL_DIR/$MODEL_OPTION $POD_DIR > $DORADO_OUTPUT_FASTQ   
else 
    echo "Fastq file already exists"
fi

if [[ $DORADO_OUTPUT_FASTQ ]]; then
    echo "Next step, aligning..."
    $DORADO aligner $REFERENCE $DORADO_OUTPUT_FASTQ > $DORADO_OUTPUT_BAM
else
    echo "Fastq file not created, error at basecalling step."
fi

if [[ $DORADO_OUTPUT_BAM ]]; then
    echo "Bam file successfully created, sorting and indexing..."

    ## Need samtools 1.9 as per https://github.com/philres/ngmlr/issues/82
    module load roslin/samtools/1.9
    samtools sort -@ 16 -o $DORADO_OUTPUT_SORTED_BAM $DORADO_OUTPUT_BAM
    samtools index -@ 16 $DORADO_OUTPUT_SORTED_BAM
    samtools flagstat $DORADO_OUTPUT_SORTED_BAM > $DORADO_OUTPUT_SORTED_BAM_STATS
    rm $DORADO_OUTPUT_BAM

else
    echo "Bam file not created, error at aligning step."
fi


## Early June 2023:
## Throws an error = > Output records written: 0terminate called after throwing an instance of 'std::runtime_error'
## This occurs when memory is not enough

