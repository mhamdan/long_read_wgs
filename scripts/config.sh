DORADO=/exports/igmm/eddie/Glioblastoma-WGS/scripts/dorado-0.3.0-linux-x64/bin/dorado
POD_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/raw/pod5/${ID}/
DORADO_OUTPUT_DIR=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/raw/fastq/dorado/${ID}
DORADO_OUTPUT_FASTQ=$DORADO_OUTPUT_DIR/${ID}.fastq
DORADO_OUTPUT_BAM=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/bams/dorado/${ID}/${ID}.bam
DORADO_OUTPUT_SORTED_BAM=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/bams/dorado/${ID}/${ID}_sorted.bam
DORADO_OUTPUT_SORTED_BAM_STATS=/exports/igmm/eddie/Glioblastoma-WGS/WGS/ONT/bams/dorado/${ID}/${ID}_sorted.bam.stats.out
MODEL_DIR=/exports/igmm/eddie/Glioblastoma-WGS/scripts/dorado-0.3.0-linux-x64/models
REFERENCE=/exports/igmm/eddie/Glioblastoma-WGS/resources/refgenome38/hg38.fa    

MOSDEPTH=/exports/igmm/eddie/Glioblastoma-WGS/anaconda/envs/py2/bin/mosdepth
